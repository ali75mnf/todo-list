package fardap.todolist.base.ExceptionHandling;


import fardap.todolist.base.customExceptions.AlreadyExistException;
import fardap.todolist.base.customExceptions.EmptyInputException;
import fardap.todolist.base.customExceptions.ServiceException;
import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.security.auth.login.FailedLoginException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;

@ControllerAdvice
public class ExeceptionHandling extends ResponseEntityExceptionHandler {


    //************************************************  EmptyInputException ************************************************
    @ExceptionHandler(EmptyInputException.class)
    public ResponseEntity<Object> handleEmptyInputException(EmptyInputException e, WebRequest request) {
        HashMap<String, Object> body = bodyOfResponse(e.getMessage(), e.toString(), HttpStatus.BAD_REQUEST,request);
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }


    //************************************************  NotFoundException ************************************************
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Object> handleNotFoundException(NotFoundException e,WebRequest request) {
        HashMap<String, Object> body = bodyOfResponse(e.getMessage(), e.toString(), HttpStatus.NOT_FOUND,request);
        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }


    //************************************************  FailedLoginException ************************************************
    @ExceptionHandler(FailedLoginException.class)
    public ResponseEntity<Object> handleFailedLoginException(FailedLoginException e,WebRequest request) {
        HashMap<String, Object> body = bodyOfResponse(e.getMessage(), e.toString(), HttpStatus.NOT_ACCEPTABLE,request);
        return new ResponseEntity<>(body, HttpStatus.NOT_ACCEPTABLE);
    }


    //************************************************   ServiceException ************************************************
    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<Object> handleServiceException(ServiceException e,WebRequest request) {
        HashMap<String, Object> body = bodyOfResponse(e.getMessage(), e.toString(), HttpStatus.BAD_REQUEST,request);
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }


    //************************************************  EmptyInputException ************************************************
    @ExceptionHandler(AlreadyExistException.class)
    public ResponseEntity<Object> handleAlreadyExistException(AlreadyExistException e,WebRequest request) {
        HashMap<String, Object> body = bodyOfResponse(e.getMessage(), e.toString(), HttpStatus.BAD_REQUEST,request);
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }


    //************************************************  bodyOfResponse ************************************************
    public static HashMap<String, Object> bodyOfResponse(String message
            , String exception, HttpStatus status,WebRequest request) {
        HashMap<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", message);
        body.put("Exception", exception);
        body.put("status", status);
        body.put("path",request.getDescription(false));
        return body;
    }
}
