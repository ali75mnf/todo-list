package fardap.todolist.base.customExceptions;

import fardap.todolist.category.categoryController.CategoryCreateModel;
import fardap.todolist.plannerUser.plannerController.PlannerCreateModel;
import fardap.todolist.plannerUser.plannerController.PlannerUpdateModel;
import fardap.todolist.task.TaskController.TaskCreateModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

@Component
@Data
@AllArgsConstructor
public class EmptyInputException extends RuntimeException {

    public EmptyInputException(String message) {
        super(message);
    }


    public static void checkEmptyServiceException(PlannerCreateModel plannerCreateModel) {
        if (plannerCreateModel.getPlannerName().isEmpty() || plannerCreateModel.getPlannerName() == null
                || plannerCreateModel.getPlannerPassword().isEmpty() || plannerCreateModel.getPlannerPassword() == null
        ) {
            throw new EmptyInputException("username or password is empty,please fill them.");
        }
    }




    public static void checkEmptyServiceException(PlannerUpdateModel plannerUpdateModel) {
        if (plannerUpdateModel.getOldPlannerName().isEmpty() || plannerUpdateModel.getOldPlannerName() == null
                || plannerUpdateModel.getOldPassword().isEmpty() || plannerUpdateModel.getOldPassword() == null
        ) {
            throw new EmptyInputException("username or password is empty,please fill them.");
        }
    }




    public static void checkEmptyServiceException(TaskCreateModel taskCreateModel) {
        if (
                taskCreateModel.getCategory_id() == null || taskCreateModel.getTaskDate() == null ||
                        taskCreateModel.getTitle().isEmpty()) {
            throw new EmptyInputException("title , date or category is empty,please fill them.");
        }
    }




    public static void checkEmptyServiceException(String title) {
        if (title.isEmpty()) throw new EmptyInputException
                ("title is empty ,please fill it.");
    }




    public static void checkEmptyServiceException(Integer id) {
        if (id == null) throw new EmptyInputException("id  is empty,please fill it.");
    }




    public static void checkEmptyServiceException(CategoryCreateModel categoryCreateModel) {
        if (categoryCreateModel.getTitle().isEmpty() || categoryCreateModel.getTitle() == null
                || categoryCreateModel.getParentCate_id() == null) {
            throw new EmptyInputException("title or parentCate_id is empty,please fill them");
        }
    }
}


