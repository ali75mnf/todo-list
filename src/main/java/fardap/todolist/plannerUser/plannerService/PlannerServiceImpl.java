package fardap.todolist.plannerUser.plannerService;

import fardap.todolist.base.customExceptions.AlreadyExistException;
import fardap.todolist.base.customExceptions.EmptyInputException;
import fardap.todolist.base.customExceptions.ServiceException;
import fardap.todolist.plannerUser.plannerController.PlannerCreateModel;
import fardap.todolist.plannerUser.plannerController.PlannerUpdateModel;
import fardap.todolist.plannerUser.plannerPersistance.PlannerRepository;
import fardap.todolist.plannerUser.plannerPersistance.PlannerUser;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.security.auth.login.FailedLoginException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlannerServiceImpl implements PlannerService {
    private PlannerRepository plannerRepository;

    @Autowired
    public PlannerServiceImpl(PlannerRepository plannerRepository) {
        this.plannerRepository = plannerRepository;
    }


    //************************************************  create  ************************************************
    @Override
    @Transactional
    public PlannerCreateModel create(PlannerCreateModel plannerCreateModel) throws ServiceException {
        EmptyInputException.checkEmptyServiceException(plannerCreateModel.getPlannerName());
        if (plannerRepository.findByPlannerName(plannerCreateModel.getPlannerName()).isPresent()) {
            throw new AlreadyExistException("the username is already exist,please try something else");
        }
        PlannerCreateModel plannerModel = new PlannerCreateModel();
        try {
            plannerModel = convertPlannerToCreatModel(
                    plannerRepository.save(convertCreatModelToPlanner(plannerCreateModel)));
        } catch (Exception e) {
            throw new ServiceException("sorry,there is a problem when adding the user ;" + e.getMessage());
        }
        return plannerModel;
    }


    //************************************************  login  ************************************************
    @Override
    public String login(PlannerCreateModel plannerCreateModel) throws FailedLoginException {
        EmptyInputException.checkEmptyServiceException(plannerCreateModel);
        String result = "";
        try {
            System.out.println(findByPlannerNameAndPassword(plannerCreateModel));
            if (findByPlannerNameAndPassword(plannerCreateModel) != null) {
                result = "you logged in";
            }
        } catch (Exception e) {
            throw new FailedLoginException
                    ("sorry you could not logged in,check your information and try again" + e.getMessage());
        }
        return result;
    }


    //************************************************  find by id  ************************************************

    @Override
    public PlannerCreateModel findById(Integer id) throws ServiceException {
        EmptyInputException.checkEmptyServiceException(id);
        PlannerCreateModel plannerCreateModel;
        try {
            plannerCreateModel = convertPlannerToCreatModel(plannerRepository.findById(id).orElseThrow(() -> new NotFoundException
                    ("the id does not exist")));
        } catch (Exception e) {
            throw new ServiceException("can't find the user : " + e.getMessage());
        }
        return plannerCreateModel;
    }


    //************************************************  edit  ************************************************
    @Override
    @Transactional
    public PlannerCreateModel edit(PlannerUpdateModel plannerUpdateModel) throws ServiceException {
        EmptyInputException.checkEmptyServiceException(plannerUpdateModel);
        PlannerCreateModel plannerModel = new PlannerCreateModel();
        try {
            PlannerUser plannerUser = plannerRepository.findByPlannerNameAndPlannerPassword
                    (plannerUpdateModel.getOldPlannerName(), plannerUpdateModel.getOldPassword()).orElseThrow(() ->
                    new NotFoundException("Wrong username or password"));
            if (!plannerUpdateModel.getPlannerName().equals("")
                    && plannerUpdateModel.getPlannerName() != null) {
                plannerUser.setPlannerName(plannerUpdateModel.getPlannerName());
            }
            if (!plannerUpdateModel.getPlannerPassword().equals("")
                    && plannerUpdateModel.getPlannerPassword() != null) {
                plannerUser.setPlannerPassword(plannerUpdateModel.getPlannerPassword());
            }
            if (!plannerUpdateModel.getEmail().equals("")
                    && plannerUpdateModel.getEmail() != null) {

                plannerUser.setEmail(plannerUpdateModel.getEmail());
            }
            plannerModel = convertPlannerToCreatModel(plannerRepository.save(plannerUser));
        } catch (Exception e) {
            throw new ServiceException("can't edit your information,please try again : " + e.getMessage());
        }
        return plannerModel;
    }


    //************************************************  find  ************************************************
    @Override
    public PlannerCreateModel findByPlannerNameAndPassword(PlannerCreateModel plannerCreateModel) throws Exception {
        EmptyInputException.checkEmptyServiceException(plannerCreateModel);
        PlannerCreateModel planner = new PlannerCreateModel();
        try {
            planner = convertPlannerToCreatModel(plannerRepository.
                    findByPlannerNameAndPlannerPassword(plannerCreateModel.getPlannerName(),
                            plannerCreateModel.getPlannerPassword()).
                    orElseThrow(() -> new NotFoundException("")));
        } catch (NotFoundException e) {
            throw new NotFoundException("Wrong username or password ");
        } catch (Exception e) {
            throw new ServiceException("can't find the user with this username and password : " + e.getMessage());
        }
        return planner;
    }


    //************************************************  delete  ************************************************
    @Override
    @Transactional
    public String delete(PlannerCreateModel plannerCreateModel) throws ServiceException {
        EmptyInputException.checkEmptyServiceException(plannerCreateModel);
        String result = "";
        try {
            plannerRepository.deleteByPlannerNameAndPlannerPassword(
                    plannerCreateModel.getPlannerName(), plannerCreateModel.getPlannerPassword());

        } catch (Exception e) {
            throw new ServiceException("can't delete the user : " + e.getMessage());
        }
        return result;
    }


    //************************************************  deleteAll  ************************************************
    @Override
    @Transactional
    public String deleteAll() throws Exception {
        try {
            plannerRepository.deleteAll();
        } catch (Exception e) {
            throw new ServiceException("can't delete the users : " + e.getMessage());
        }
        return "deleted All";
    }


    //************************************************  findAll  ************************************************
    @Override
    public List<PlannerCreateModel> findAll() throws Exception {
        List<PlannerCreateModel> plannerModels;
        try {
            plannerModels = plannerRepository.findAll().stream()
                    .map(PlannerServiceImpl::convertPlannerToCreatModel)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new ServiceException("can't find the users" + e.getMessage());
        }
        return plannerModels;
    }


    //************************************************  converter methods  ************************************************
    public static PlannerCreateModel convertPlannerToCreatModel(PlannerUser plannerUser) {
        return new PlannerCreateModel(plannerUser.getId(), plannerUser.getPlannerName(),
                plannerUser.getPlannerPassword(), plannerUser.getEmail());
    }

    public static PlannerUser convertCreatModelToPlanner(PlannerCreateModel plannerCreateModel) {
        return new PlannerUser(plannerCreateModel.getPlannerName(), plannerCreateModel.getPlannerPassword(),
                plannerCreateModel.getEmail()
        );
    }
}