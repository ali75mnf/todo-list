package fardap.todolist.plannerUser.plannerService;

import fardap.todolist.plannerUser.plannerController.PlannerCreateModel;
import fardap.todolist.plannerUser.plannerController.PlannerUpdateModel;
import javassist.NotFoundException;

import javax.security.auth.login.FailedLoginException;
import java.util.List;


public interface PlannerService {
    PlannerCreateModel create(PlannerCreateModel plannerRegistrationModel) throws Exception;

    String login(PlannerCreateModel plannerCreateModel) throws NotFoundException, FailedLoginException;

    PlannerCreateModel edit(PlannerUpdateModel PlannerUpdateModel);

    PlannerCreateModel findById(Integer id) throws Exception;

    PlannerCreateModel findByPlannerNameAndPassword(PlannerCreateModel plannerCreateModel) throws Exception;

    String delete(PlannerCreateModel plannerRegistrationModel) throws Exception;

    String deleteAll() throws Exception;

    List<PlannerCreateModel> findAll() throws Exception;

}
