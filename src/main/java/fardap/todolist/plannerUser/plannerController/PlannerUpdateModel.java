package fardap.todolist.plannerUser.plannerController;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlannerUpdateModel {
    private Integer id;
    private String plannerName;
    private String plannerPassword;
    private String email;
    private String oldPassword;
    private String oldPlannerName;

}
