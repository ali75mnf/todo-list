package fardap.todolist.plannerUser.plannerController;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PlannerCreateModel {
    private Integer id;
    private String plannerName;
    private String plannerPassword;
    private String email;

    public PlannerCreateModel(int id, String plannerName, String plannerPassword, String email) {
        this.id = id;
        this.plannerName = plannerName;
        this.plannerPassword = plannerPassword;
        this.email = email;
    }


}
