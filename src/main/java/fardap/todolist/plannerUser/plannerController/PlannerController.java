package fardap.todolist.plannerUser.plannerController;

import fardap.todolist.plannerUser.plannerService.PlannerServiceImpl;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.FailedLoginException;
import java.util.List;

@Controller
@Data
@RequestMapping("/plannerUser")
public class PlannerController {

    private PlannerServiceImpl plannerService;

    @Autowired
    public PlannerController(PlannerServiceImpl plannerService) {
        this.plannerService = plannerService;
    }


    //************************************************  create  ************************************************
    @PostMapping("/createUser")
    public @ResponseBody
    ResponseEntity<?> create(@RequestBody PlannerCreateModel plannerCreateModel) throws Exception {
        PlannerCreateModel plannerModel = plannerService.create(plannerCreateModel);
        return new ResponseEntity(plannerModel, HttpStatus.CREATED);
    }


    //************************************************  login  ************************************************
    @PostMapping("/login")
    public @ResponseBody
    ResponseEntity<?> login(@RequestBody PlannerCreateModel plannerCreateModel) throws FailedLoginException {
        String result = plannerService.login(plannerCreateModel);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    //************************************************  find  ************************************************
    @GetMapping("/find/{id}")
    public @ResponseBody
    ResponseEntity<?> find(@PathVariable Integer id) {
        PlannerCreateModel plannerCreateModel = plannerService.findById(id);
        return new ResponseEntity<>(plannerCreateModel, HttpStatus.FOUND);
    }


    //************************************************  edit  ************************************************
    @PutMapping("/edit")
    public @ResponseBody
    ResponseEntity<?> edit(@RequestBody PlannerUpdateModel plannerUpdateModel) {
        PlannerCreateModel plannerModel = plannerService.edit(plannerUpdateModel);
        return new ResponseEntity<>(plannerModel, HttpStatus.OK);
    }


    //************************************************  delete  ************************************************
    @DeleteMapping("/delete")
    public @ResponseBody
    ResponseEntity<?> delete(@RequestBody PlannerCreateModel plannerCreateModel) {
        String result = plannerService.delete(plannerCreateModel);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    //************************************************  deleteAll  ************************************************
    @DeleteMapping("/deleteAll")
    @ResponseBody
    ResponseEntity<?> deleteAll() throws Exception {
        String result = plannerService.deleteAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    //************************************************  findAll  ************************************************
    @GetMapping("/findAll")
    @ResponseBody
    ResponseEntity<?> findAll() throws Exception {
        List<PlannerCreateModel> plannerModels = plannerService.findAll();
        return new ResponseEntity<>(plannerModels, HttpStatus.FOUND);
    }


}