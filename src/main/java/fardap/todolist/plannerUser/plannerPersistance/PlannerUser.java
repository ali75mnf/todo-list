package fardap.todolist.plannerUser.plannerPersistance;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fardap.todolist.base.Configuration.RunConfiguration;
import fardap.todolist.task.taskPersistance.Tasks;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "planner_user", schema = RunConfiguration.DB,
        indexes = {@Index(name = "plannerName_index", columnList = "planner_name", unique = true),
        @Index(name = "plannerName_index", columnList = "planner_password")})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlannerUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "planner_name", nullable = false, unique = true)
    private String plannerName;
    @Column(name = "planner_password", nullable = false)
    private String plannerPassword;
    @Column(name = "email")
    private String email;
    @OneToMany(mappedBy = "plannerUser", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Tasks> tasksList;

    public PlannerUser(String plannerName, String plannerPassword, String email) {
        this.plannerName = plannerName;
        this.plannerPassword = plannerPassword;
        this.email = email;
    }

    public PlannerUser(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "PlannerUser{" +
                "id=" + id +
                ", plannerName='" + plannerName + '\'' +
                ", plannerPassword='" + plannerPassword + '\'' +
                ", email='" + email + '\'' +
                ", tasksList=" + tasksList +
                '}';
    }
}
