package fardap.todolist.plannerUser.plannerPersistance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlannerRepository extends JpaRepository<PlannerUser, Integer> {
    Optional<PlannerUser> findByPlannerNameAndPlannerPassword(String name, String password);

    Optional<PlannerUser> findByPlannerName(String name);

    Integer deleteByPlannerNameAndPlannerPassword(String plannerName, String password);

}
