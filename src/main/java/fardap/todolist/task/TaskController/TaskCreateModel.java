package fardap.todolist.task.TaskController;
import fardap.todolist.task.taskEnums.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskCreateModel {
    private Integer id;
    private String title;
    private String description;
    private LocalDate taskDate;
    private TaskStatus taskStatus;
    private Integer category_id;
    private Integer plannerUser_id;

    }
