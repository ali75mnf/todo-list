package fardap.todolist.task.TaskController;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskPostponeModel {
    private Integer id;
    private LocalDate date;
}
