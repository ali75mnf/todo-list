package fardap.todolist.task.TaskController;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskStatusModel {
private Integer id;
private Boolean statusCheck;
}
