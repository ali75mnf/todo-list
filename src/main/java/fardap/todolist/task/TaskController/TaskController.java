package fardap.todolist.task.TaskController;

import fardap.todolist.task.taskService.TaskServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/tasks")
public class TaskController {
    private TaskServiceImpl taskService;
@Autowired
    public TaskController(TaskServiceImpl taskService) {
        this.taskService = taskService;
    }




    //************************************************  create  ************************************************
    @PostMapping("/create")
    public @ResponseBody ResponseEntity<?> create(@RequestBody TaskCreateModel taskCreateModel) throws Exception {
    TaskCreateModel taskModel= taskService.create(taskCreateModel);
    return new ResponseEntity<>(taskModel,HttpStatus.CREATED);
    }




    //************************************************  find  ************************************************
    @GetMapping("/find/{id}")
    public @ResponseBody ResponseEntity<?> find(@PathVariable Integer id ) throws Exception {
    TaskCreateModel taskCreateModel=taskService.findById(id);
    return new ResponseEntity<>(taskCreateModel,HttpStatus.FOUND);
    }



    //************************************************  edit  ************************************************
    @PutMapping("/edit")
    public @ResponseBody ResponseEntity<?> edit(@RequestBody TaskCreateModel taskCreateModel) throws Exception {
        TaskCreateModel taskModel=taskService.edit(taskCreateModel);
        return new ResponseEntity<>(taskModel,HttpStatus.OK);
    }




    //************************************************  delete  ************************************************
    @DeleteMapping("/delete/{id}")
    public @ResponseBody ResponseEntity<?> delete(@PathVariable Integer id){
        String result=taskService.delete(id);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }



    //************************************************  find All ************************************************
    @GetMapping("/findAll")
    public @ResponseBody ResponseEntity<?> findAll(){
        List<TaskCreateModel> taskModels=taskService.findAll();
        return new ResponseEntity<>(taskModels,HttpStatus.FOUND);
    }



    //************************************************  delete All  ************************************************
    @DeleteMapping("/deleteAll")
    public @ResponseBody ResponseEntity<?> deleteAll(){
        String result=taskService.deleteAll();
        return new ResponseEntity<>(result,HttpStatus.OK);
    }



    //************************************************  postpone  ************************************************
@PutMapping("/postpone")
@ResponseBody ResponseEntity<?> postpone(@RequestBody TaskPostponeModel postponeModel) throws Exception {
   TaskCreateModel taskModel=taskService.postpone(postponeModel);
    return new ResponseEntity<>(taskModel,HttpStatus.OK);
}



    //************************************************  change status   ************************************************
@PutMapping("/changeStatus")
@ResponseBody ResponseEntity<?> changeStatus(@RequestBody TaskStatusModel taskStatusModel) throws Exception {
    TaskCreateModel taskModel=taskService.changStatus(taskStatusModel);
    return new ResponseEntity<>(taskModel,HttpStatus.OK);
}


}
