package fardap.todolist.task.taskService;


import fardap.todolist.task.TaskController.TaskCreateModel;
import fardap.todolist.task.TaskController.TaskPostponeModel;
import fardap.todolist.task.TaskController.TaskStatusModel;

import java.util.List;

public interface TaskService {
    TaskCreateModel create(TaskCreateModel taskCreateModel) throws Exception;

    TaskCreateModel edit(TaskCreateModel taskCreateModel) throws Exception;

    String delete(Integer id) throws Exception;

    TaskCreateModel findById(Integer id) throws Exception;

    List<TaskCreateModel> findAll() throws Exception;

    String deleteAll();

    TaskCreateModel postpone(TaskPostponeModel taskPostponeModel) throws Exception;

    TaskCreateModel changStatus(TaskStatusModel taskStatusModel) throws Exception;
}
