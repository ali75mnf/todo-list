package fardap.todolist.task.taskService;

import fardap.todolist.base.customExceptions.EmptyInputException;
import fardap.todolist.base.customExceptions.ServiceException;
import fardap.todolist.category.categoryController.CategoryCreateModel;
import fardap.todolist.category.categoryPersistance.CategoryRepository;
import fardap.todolist.category.categoryPersistance.Category;
import fardap.todolist.category.categorySevice.CategoryServiceImpl;
import fardap.todolist.plannerUser.plannerController.PlannerCreateModel;
import fardap.todolist.plannerUser.plannerPersistance.PlannerRepository;
import fardap.todolist.plannerUser.plannerPersistance.PlannerUser;
import fardap.todolist.plannerUser.plannerService.PlannerServiceImpl;
import fardap.todolist.task.TaskController.TaskCreateModel;
import fardap.todolist.task.TaskController.TaskPostponeModel;
import fardap.todolist.task.TaskController.TaskStatusModel;
import fardap.todolist.task.taskEnums.TaskStatus;
import fardap.todolist.task.taskPersistance.TaskRepository;
import fardap.todolist.task.taskPersistance.Tasks;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl implements TaskService {

    private TaskRepository taskRepository;
    private CategoryRepository categoryRepository;
    private PlannerRepository plannerRepository;

    @Autowired
    public TaskServiceImpl(TaskRepository taskRepository,
                           CategoryRepository categoryRepository, PlannerRepository plannerRepository) {
        this.taskRepository = taskRepository;
        this.categoryRepository = categoryRepository;
        this.plannerRepository = plannerRepository;
    }




    //************************************************  create  ************************************************
    @Override
    @Transactional
    public TaskCreateModel create(TaskCreateModel taskCreateModel) throws Exception {
        EmptyInputException.checkEmptyServiceException(taskCreateModel);
        TaskCreateModel taskModel=new TaskCreateModel();
        try {

            CategoryCreateModel categoryCreateModel = CategoryServiceImpl.convertCategoryToCategoryCreateModel

                    (categoryRepository.findById(taskCreateModel.getCategory_id()).orElseThrow(() ->
                            new Exception("category id does not exist")));
            taskCreateModel.setCategory_id(categoryCreateModel.getId());


            PlannerCreateModel plannerCreateModel = PlannerServiceImpl.convertPlannerToCreatModel
                    (plannerRepository.findById(taskCreateModel.getPlannerUser_id()).
                            orElseThrow(() -> new Exception("usr id doesn not exist")));
            taskCreateModel.setPlannerUser_id(plannerCreateModel.getId());
            taskCreateModel.setTaskStatus(TaskStatus.undone);


           taskModel= convertTaskToTaskCreateModel(
                   taskRepository.save(convertTaskCreateModelToTask(taskCreateModel)));
        }catch (NotFoundException e){
            throw new NotFoundException("can't find the user or category : "+e.getMessage());
        }
        catch (Exception e) {
            throw new ServiceException
                    ("sorry,there is a problem when adding the task ;" +e.getMessage());
        }

        return taskModel;
    }


    //************************************************  find by id  ************************************************
    @Override
    public TaskCreateModel findById(Integer id) throws Exception {
        TaskCreateModel taskCreateModel;
        try {
            taskCreateModel = convertTaskToTaskCreateModel(
                    taskRepository.findById(id).orElseThrow(() -> new NotFoundException("can't find the category ")));
        } catch (Exception e) {
            throw new ServiceException("can't find the category : " + e.getMessage());
        }
        return taskCreateModel;
    }


    //************************************************  edit  ************************************************
    @Override
    @Transactional
    public TaskCreateModel edit(TaskCreateModel taskCreateModel) throws Exception {
        TaskCreateModel taskModel=new TaskCreateModel();
        try {
            taskModel = findById(taskCreateModel.getId());
            if (taskCreateModel.getTitle()!=null){ taskModel.setTitle(taskCreateModel.getTitle());}
            if (taskCreateModel.getCategory_id()!=null){taskModel.setCategory_id(taskCreateModel.getCategory_id());}
            if (taskCreateModel.getDescription()!=null){taskModel.setDescription(taskCreateModel.getDescription());}
            if (taskCreateModel.getTaskStatus()!=null){taskModel.setTaskStatus(taskCreateModel.getTaskStatus());}
            if (taskCreateModel.getTaskDate()!=null) {taskModel.setTaskDate(taskCreateModel.getTaskDate());}
            taskRepository.save(convertTaskCreateModelToTask(taskModel));
        }catch (NotFoundException e) {
            throw new NotFoundException("can't find the task ");
        }
        catch (Exception e) {
            throw new ServiceException("can't edit the task : "+e.getMessage());
        }
        return taskModel;
    }


    //************************************************  delete  ************************************************
    @Override
    @Transactional
    public String delete(Integer id) throws ServiceException {
        EmptyInputException.checkEmptyServiceException(id);
        try {
            taskRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("could'nt delete the task : "+ e.getMessage());
        }
        return "deleted successfully";
    }


    //************************************************  find All  ************************************************
    @Override
    public List<TaskCreateModel> findAll() throws ServiceException {
        List<TaskCreateModel> taskCreateModels = new ArrayList<>();
        try {
            taskCreateModels = taskRepository.findAll().stream().map(TaskServiceImpl::convertTaskToTaskCreateModel).
                    collect(Collectors.toList());
        } catch (Exception e) {
            throw new ServiceException("sorry can't find all tasks :" + e.getMessage());
        }
        return taskCreateModels;
    }


    //************************************************  delete All  ************************************************
    @Override
    @Transactional
    public String deleteAll() {
        try {
            taskRepository.deleteAll();
        } catch (Exception e) {
            throw new EmptyInputException("sorry can't delete all tasks :" + e.getMessage());
        }
        return "all tasks deleted";
    }


    //************************************************  postpone  ************************************************


    @Override
    @Transactional
    public TaskCreateModel postpone(TaskPostponeModel taskPostponeModel) throws Exception {
        Tasks tasks=new Tasks();
        try {
           tasks =convertTaskCreateModelToTask(findById(taskPostponeModel.getId()));
            tasks.setTaskDate(taskPostponeModel.getDate());
            tasks.setTaskStatus(TaskStatus.undone);
           taskRepository.save(tasks);
        }catch (NotFoundException e){
            throw new NotFoundException("can't find the task");
        }
        catch (Exception e) {
            throw new ServiceException("can't postpone the task : "+e.getMessage());
        }
        return convertTaskToTaskCreateModel(tasks);
    }


    //************************************************  changStatus  ************************************************

    @Override
    public TaskCreateModel changStatus(TaskStatusModel taskStatusModel) throws Exception {
        Tasks tasks=new Tasks();
        try {
            tasks=convertTaskCreateModelToTask(findById(taskStatusModel.getId()));
            if (taskStatusModel.getStatusCheck()){tasks.setTaskStatus(TaskStatus.done);}else
                tasks.setTaskStatus(TaskStatus.undone);
            taskRepository.save(tasks);
        }catch (NotFoundException e){
            throw new NotFoundException("can't find the task");
        } catch (Exception e) {
            throw new ServiceException("can't change the status of the task : "+e.getMessage());
        }
        return convertTaskToTaskCreateModel(tasks);
    }


    //************************************************  converter methods  ************************************************
    public static Tasks convertTaskCreateModelToTask(TaskCreateModel taskCreateModel) {

        return new Tasks(taskCreateModel.getId(),
                taskCreateModel.getTitle(),
                taskCreateModel.getDescription(),
                taskCreateModel.getTaskDate(),
                taskCreateModel.getTaskStatus(),new PlannerUser(taskCreateModel.getPlannerUser_id()),
                new Category(taskCreateModel.getCategory_id()));
    }

    public static TaskCreateModel convertTaskToTaskCreateModel(Tasks tasks) {
        return new TaskCreateModel(tasks.getId(),
                tasks.getTitle(),
                tasks.getDescription(),
                tasks.getTaskDate(),
                tasks.getTaskStatus(),tasks.getCategory().getId(),tasks.getPlannerUser().getId());
    }
}
