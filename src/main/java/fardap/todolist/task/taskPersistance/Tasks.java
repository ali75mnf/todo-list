package fardap.todolist.task.taskPersistance;

import fardap.todolist.base.Configuration.RunConfiguration;
import fardap.todolist.category.categoryPersistance.Category;
import fardap.todolist.plannerUser.plannerPersistance.PlannerUser;
import fardap.todolist.task.taskEnums.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name = "tasks_table",schema = RunConfiguration.DB
//        ,indexes = @Index(name = "title_index",columnList = "tasks_title")
)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tasks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "tasks_title")
    private String title;
    @Column(name = "task_description")
    private String description;
    @Column(name = "task_start_date")
    private LocalDate taskDate;
    @Column(name = "task_status")
    private TaskStatus taskStatus=TaskStatus.undone;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",referencedColumnName ="id")
    private PlannerUser plannerUser;
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "category_id",referencedColumnName ="id")
    private Category category;


    @Override
    public String toString() {
        return "Tasks{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", taskDate=" + taskDate +
                ", taskStatus=" + taskStatus +
                ", category=" + category +
                '}';
    }
}
