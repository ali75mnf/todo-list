package fardap.todolist.category.categorySevice;

import fardap.todolist.base.customExceptions.EmptyInputException;
import fardap.todolist.base.customExceptions.ServiceException;
import fardap.todolist.category.categoryController.CategoryCreateModel;
import fardap.todolist.category.categoryController.CategoryEditModel;
import fardap.todolist.category.categoryPersistance.CategoryRepository;
import fardap.todolist.category.categoryPersistance.Category;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {
    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    //************************************************  create ************************************************
    @Override
    @Transactional
    public CategoryCreateModel create(CategoryCreateModel categoryCreateModel) throws ServiceException {
        EmptyInputException.checkEmptyServiceException(categoryCreateModel);
        Category categoryModel = new Category();
        try {
            categoryModel = categoryRepository.save(convertCategoryCreateModelToCategory(categoryCreateModel));
        } catch (Exception e) {
            throw new ServiceException("can't create the category : " + e.getMessage());
        }
        return convertCategoryToCategoryCreateModel(categoryModel);
    }


    //************************************************  find by id ************************************************

    @Override
    public CategoryCreateModel findById(Integer id) throws Exception {
        EmptyInputException.checkEmptyServiceException(id);
        CategoryCreateModel categoryCreateModel = new CategoryCreateModel();
        try {
            categoryCreateModel = convertCategoryToCategoryCreateModel(categoryRepository.findById(id).orElseThrow(
                    () -> new NotFoundException("the id does not exist")));
        } catch (NotFoundException e) {
            throw new NotFoundException(e.getMessage());
        } catch (Exception e) {
            throw new ServiceException("can't find the category with this id " + e.getMessage());
        }
        return categoryCreateModel;
    }


    //************************************************  edit ************************************************
    @Override
    @Transactional
    public CategoryCreateModel edit(CategoryEditModel categoryEditModel) throws Exception {
        EmptyInputException.checkEmptyServiceException(categoryEditModel.getId());
        CategoryCreateModel categoryModel = convertCategoryToCategoryCreateModel(categoryRepository.findById(categoryEditModel.getId()).
                orElseThrow(() -> new NotFoundException("there is not such a category")));
        try {
            if (categoryEditModel.getTitle() != null) {
                categoryModel.setTitle(categoryEditModel.getTitle());
            }
            if (categoryEditModel.getParentCate_id() != null) {
                categoryModel.setParentCate_id(categoryEditModel.getParentCate_id());
            }
            if (categoryEditModel.getSubCategory() != null) {
                categoryModel.setSubCategory(categoryEditModel.getSubCategory());
            }
            categoryModel = convertCategoryToCategoryCreateModel(
                    categoryRepository.save(convertCategoryCreateModelToCategory(categoryModel)));
        } catch (Exception e) {
            throw new ServiceException("can't edit the  category ;" + e.getMessage());
        }
        return categoryModel;
    }


    //************************************************  findAll ************************************************
    @Override
    public List<CategoryCreateModel> findAll() throws ServiceException {
        List<CategoryCreateModel> categories = new ArrayList<>();
        try {
            categories = categoryRepository.findAll().stream().map(CategoryServiceImpl::convertCategoryToCategoryCreateModel)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new ServiceException("can't find the  categories ;" + e.getMessage());
        }
        return categories;
    }


    //************************************************  delete ************************************************
    @Override
    @Transactional
    public String delete(Integer id) throws Exception {
        EmptyInputException.checkEmptyServiceException(id);
        if (findById(id) == null)
            throw new EmptyInputException("there is not such a category ");
        try {
            categoryRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("can't delete the category : " + e.getMessage());
        }
        return "deleted";
    }


    //************************************************  delete All ************************************************
    @Override
    @Transactional
    public String deleteAll() throws ServiceException {
        try {
            categoryRepository.deleteAll();
        } catch (Exception e) {
            throw new ServiceException("can't delete the categories : "+e.getMessage());
        }
        return "all of categories deleted";
    }



    //************************************************  find all by title ************************************************

    @Override
    public List<CategoryCreateModel> findAllByTitle(String title) throws NotFoundException {
        EmptyInputException.checkEmptyServiceException(title);
       List<CategoryCreateModel> categoryModels=new ArrayList<>();
        try {
           if (categoryRepository.findAllByTitle(title).isEmpty())throw new
                   NotFoundException("can't find the category with this title");
           else categoryModels=categoryRepository.findAllByTitle(title).stream().
                   map(CategoryServiceImpl::convertCategoryToCategoryCreateModel).collect(Collectors.toList());

        } catch (NotFoundException e) {
            throw new NotFoundException(e.getMessage());
        } catch (Exception e) {
            throw new ServiceException("can't find the category with this title " + e.getMessage());
        }
        return categoryModels;
    }


    //************************************************  convert methods ************************************************

    public static Category convertCategoryCreateModelToCategory(CategoryCreateModel categoryCreateModel) {
        return new Category(categoryCreateModel.getId(), categoryCreateModel.getTitle(),
                categoryCreateModel.getTasks(), new Category(categoryCreateModel.getParentCate_id()),
                categoryCreateModel.getSubCategory());
    }

    public static CategoryCreateModel convertCategoryToCategoryCreateModel(Category category) {
        return new CategoryCreateModel(category.getId(), category.getTitle(), category.getTasks(),
                category.getParentCate().getId(), category.getSubCategory());
    }


}
