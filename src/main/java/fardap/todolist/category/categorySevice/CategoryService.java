package fardap.todolist.category.categorySevice;

import fardap.todolist.category.categoryController.CategoryCreateModel;
import fardap.todolist.category.categoryController.CategoryEditModel;
import fardap.todolist.category.categoryController.CategoryFindModel;
import javassist.NotFoundException;

import java.util.List;

public interface CategoryService {
    CategoryCreateModel create(CategoryCreateModel categoryCreateModel) throws Exception;

    CategoryCreateModel findById(Integer id) throws Exception;

    List<CategoryCreateModel> findAllByTitle(String title) throws NotFoundException;

    CategoryCreateModel edit(CategoryEditModel categoryEditModel) throws Exception;

    List<CategoryCreateModel> findAll() throws Exception;

    String delete(Integer id) throws Exception;

    String deleteAll() throws Exception;
}
