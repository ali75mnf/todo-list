package fardap.todolist.category.categoryController;

import fardap.todolist.category.categoryPersistance.Category;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryEditModel {
    private Integer id;
    private String title;
    private Integer parentCate_id;
    private List<Category> subCategory;
}
