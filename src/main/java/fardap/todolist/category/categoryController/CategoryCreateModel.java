package fardap.todolist.category.categoryController;

import fardap.todolist.category.categoryPersistance.Category;
import fardap.todolist.task.taskPersistance.Tasks;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryCreateModel {
    private Integer id;
    private String title;
    private List<Tasks> tasks;
    private Integer parentCate_id;
    private List<Category> subCategory;
}
