package fardap.todolist.category.categoryController;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CategoryFindModel {
private Integer parent_id;
private Integer id;
}
