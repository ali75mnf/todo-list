package fardap.todolist.category.categoryController;

import fardap.todolist.base.customExceptions.EmptyInputException;
import fardap.todolist.category.categorySevice.CategoryServiceImpl;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/category")
public class CategoryController {
    private CategoryServiceImpl categoryService;

    @Autowired
    public CategoryController(CategoryServiceImpl categoryService) {
        this.categoryService = categoryService;
    }





    //************************************************  create ************************************************
    @PostMapping("/create")
    public @ResponseBody
    ResponseEntity<?> create(@RequestBody CategoryCreateModel categoryCreateModel) throws Exception {
        CategoryCreateModel categoryModel;
        categoryModel = categoryService.create(categoryCreateModel);
        return new ResponseEntity<>(categoryModel, HttpStatus.CREATED);
    }





    //************************************************  find ************************************************
    @GetMapping("/find/{id}")
    public @ResponseBody
    ResponseEntity<?> find(@PathVariable Integer id) throws Exception {
        CategoryCreateModel categoryModel = categoryService.findById(id);
        return new ResponseEntity<>(categoryModel, HttpStatus.FOUND);
    }





    //************************************************  edit ************************************************
    @PutMapping("/edit")
    public @ResponseBody
    ResponseEntity<?> edit(@RequestBody CategoryEditModel categoryEditModel) throws Exception {
        CategoryCreateModel createModel = categoryService.edit(categoryEditModel);
        return new ResponseEntity<>(createModel, HttpStatus.OK);
    }



    //************************************************  find all by title ************************************************

    @GetMapping("/findAllByTitle/{title}")
    public @ResponseBody  ResponseEntity<?> findAllByTitle(@PathVariable String title) throws Exception {
        List<CategoryCreateModel> categoryModels = categoryService.findAllByTitle(title);
        return new ResponseEntity<>(categoryModels, HttpStatus.FOUND);
    }


    //************************************************  find All ************************************************
    @GetMapping("/findAll")
    @ResponseBody
    ResponseEntity<?> findAll() throws Exception {
        List<CategoryCreateModel> categoryModels = categoryService.findAll();
        return new ResponseEntity<>(categoryModels, HttpStatus.FOUND);
    }





    //************************************************  delete ************************************************
    @DeleteMapping("/delete/{id}")
    public @ResponseBody
    ResponseEntity<?> delete(@PathVariable Integer id) throws Exception {
        String result = categoryService.delete(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }





    //************************************************  delete All************************************************
    @DeleteMapping("/deleteAll")
    @ResponseBody
    ResponseEntity<?> deleteAll() throws Exception {
        String result = categoryService.deleteAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}