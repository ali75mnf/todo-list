package fardap.todolist.category.categoryPersistance;


import com.fasterxml.jackson.annotation.JsonIgnore;
import fardap.todolist.base.Configuration.RunConfiguration;
import fardap.todolist.task.taskPersistance.Tasks;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "category",schema = RunConfiguration.DB, indexes = @Index(name = "title_index", columnList = "title"))
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "title")
    private String title;
    @JsonIgnore
    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    private List<Tasks> tasks;
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "parentCate_id", referencedColumnName = "id")
    private Category parentCate;
    @JsonIgnore
    @OneToMany(mappedBy = "parentCate", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<Category> subCategory;

    public Category(int id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", tasks=" + tasks +
                '}';
    }
}
